"""
main
@author raclette
"""

from time import time


def parser(f_path):
    with open("input/"+f_path) as f:
        readline = lambda : map(int, f.readline().split())
        V, E, R, C, X = readline()
        videos = list(readline())
        end_points = []
        for _ in range(E):
            Ld, K = readline()
            cache_servers = {}
            for _ in range(K):
                idx, lc = readline()
                cache_servers[idx] = lc
            end_points.append({"serveur_lat": Ld, "cache_lat": cache_servers})
        requests = []
        for _ in range(R):
            rv, re, rn = list(readline())
            requests.append({"id": rv, "end_id": re, "nb": rn})
    return V, E, R, C, X, end_points, requests, videos


def get_latence_gain(cache_id, end_point):
    return end_point["serveur_lat"] - end_point["cache_lat"][cache_id]


def getCacheVideoInterest(cacheId, V, E, R, C, X, end_points, requests, videos):
    vect = [0 for _ in range(V)]
    request_associated = {}
    for req in requests:
        idx = req["id"]
        end_point = end_points[req["end_id"]]
        if videos[idx] <= X and cacheId in end_point["cache_lat"]:
            latence_gain = get_latence_gain(cacheId, end_point)
            vect[idx] += latence_gain*req["nb"]/videos[idx]
            request_associated.setdefault(idx, [])
            request_associated[idx].append(req)
    return vect, request_associated


def fillCache(cacheId, V, E, R, C, X, end_points, requests, videos):
    vect, request_associated  = getCacheVideoInterest(cacheId,  V, E, R, C, X, end_points, requests, videos)
    listId = sorted(range(V), key=lambda i:vect[i], reverse=True)
    listVid = []
    remainingCapa = X
    for vid in listId:
        if vect[vid] == 0:
            break
        if remainingCapa >= videos[vid]:
            listVid.append(vid)
            for a in request_associated[vid]:
                requests.remove(a)
            remainingCapa -= videos[vid]
    return listVid


def computeVids(*data):
    nb_cache = data[3]
    # def avg_latence(cache_id):
    #     count = 0
    #     summ = 0
    #     for i, end_point in enumerate(end_points):
    #         if cache_id in end_point["cache_lat"]:
    #             summ += get_latence_gain(cache_id, i, end_points)
    #             count += 1
    #     return summ / count
    # cache_idx = sorted(range(C), key=avg_latence)
    return [fillCache(cacheId, *data) for cacheId in range(nb_cache)]

paths = [
    "me_at_the_zoo.in",
    # "trending_today.in",
    # "videos_worth_spreading.in",
    # "kittens.in",
]

# Get initial time
tt = time()
for path in paths:
    input_data = parser(path)
    output = computeVids(*input_data)
    N = len(output)
    with open("output/"+path[:-3]+".out", "w") as fout:
        lines = []
        lines.append("{}\n".format(N))
        for i, server_ids in enumerate(output):
            if server_ids:
                lines.append("{} {}\n".format(i, " ".join(map(str, server_ids))))
        fout.writelines(lines)
    print(path, time()-tt)
    tt = time()
