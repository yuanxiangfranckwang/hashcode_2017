#
#   Optimizer
#
#   @author: r.olivanti

import sys
from utils.utils import access_column

def optimizer(params, domain, fun, argsFun=[]):
    """ Branch and bound minimization algorithm
    args:
    params -- list of parametres (the order is followed by the algorithm) 
    domain -- range of each param (dictionary)
    fun -- problem function [valid, score] = fun([params], argsFun)
        valid: False if one of the constraints is not met
        score: output of the cost function
    argsFun -- arguments required by fun

    return:
    res -- dictionary
            'optimum' (list)
            'score' (best score)
            'evals' (nb of calls of fun)
            'cutsConstraints' (nb of cases elimited by constraints)
            'cutsScore' (nb of cases eliminated by bounds)
    """

    # algo data
    problemDepth = len(params)
    searchDomain = []
    data = []
    currentParams = []
    bestParams = []
    for i in range(problemDepth):
        data.append([])
        currentParams.append(0)
        bestParams.append(0)

    # init searchDomain - the order of params matters !
    for param in params:
        searchDomain.append(domain[param])
       
    bestScore = sys.float_info.max
    problemSolved = False
    currentDepth = 0
    dive = True

    # perf stats
    nbEvals = 0
    nbCutsValidity = 0
    nbCutsScore = 0

    while not problemSolved:
        if dive:
            # Diving phase
            currentDepth += 1
            bestEstimate = bestScore
            dive = False
            
            # Use search domain to compute validity and the score estimates
            testVector = currentParams[0:(currentDepth-1)]
            testVector.append(0)

            for i in searchDomain[currentDepth-1]:
                testVector[-1] = i

                # try the new config 
                if argsFun:
                    [valid, score] = fun(testVector, argsFun)
                else:
                    [valid, score] = fun(testVector)
                
                nbEvals += 1
                if valid:
                    # Add the valid config to this depth
                    data[currentDepth-1].append([i, score])
                
                    # pick the best valid
                    if score < bestEstimate: 
                        bestEstimate = score
                        currentParams[currentDepth-1] = i
                        dive = True
                else:
                    nbCutsValidity += 1

            # Once the problem depth has been reached the score is no longer an estimate
            if currentDepth == problemDepth:
                dive = False
                # Update if a new minimun has been found
                if bestEstimate < bestScore:
                    bestScore = bestEstimate
                    bestParams = currentParams[:]
        else:
            # ascending phase
            # if no dive has been engaged clean the data before climbing
            data[currentDepth-1] = []
            if currentDepth == 1:
                problemSolved = True
            else:
                currentDepth -= 1
                # Find the previous value and remove it
                index = access_column(data[currentDepth-1], 0).index(currentParams[currentDepth-1])
                data[currentDepth-1].pop(index)
                for item in data[currentDepth-1]:
                    # item[1] -> score
                    if item[1] < bestScore:
                        # dive is mandatory
                        dive = True
                        currentParams[currentDepth-1] = item[0]
                        # start diving imediately
                        break
                    else:
                       nbCutsScore += 1 
            
    # Problem solved - format outPut
    return {'optimum': bestParams, 'score': bestScore, 'evals': nbEvals, \
            'cutsConstraints': nbCutsValidity, 'cutsScore': nbCutsScore}
